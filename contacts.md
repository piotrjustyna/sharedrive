| Name | Company | Role | Description |
| --- | --- | --- | --- |
| [Katie Keogh](mailto:K.Keogh@amcham.ie) | American Chamber of Commerce | Director of Member Networks | Our main contact. |
| [Brendan McDonald](mailto:Brendan.McDonald@ida.ie) | IDA | Regional Manager | 2017 hackathon winner. |
| [Maria McSweeney](mailto:mariamcs@dropbox.com) | Dropbox | Head of Support Operations | 2018 hackathon winner. |
| [Katie O'Leary](mailto:katie.oleary@dublinairport.com) | Future Factory | Head of Innovation | - |
| [Dermot O�Gara](https://www.linkedin.com/in/dermot-o-gara-2b28aa10/?originalSubdomain=ie) | NTA | Head of Public Affairs | **Piotr**: Priority contact we should reach out to. |
| [Martin D. Shanahan](https://www.linkedin.com/in/martindshanahan/) | IDA | CEO | We talked during the hackathon and received from him a list of traffic-related challenges IDA faces. |
| [Sean McNulty](https://www.linkedin.com/in/sean-mcnulty-45532a14/) | Dolmen | Chairman and Director | Great advice given during the hackathon. Could reach out to him with design problems. |
| [Declan Bogan](https://www.linkedin.com/in/declanbogan/?originalSubdomain=ie) | innovate-2-zero | Co-Founder | Great advice given during the hackathon. Could reach out to him for contacts or advice. |
| Declan Hughes | Department of Business, Enterprise and Innovation | Assistant Secretary General | He gave us great feedback on our idea and shared many traffic challenges the government faces. **Piotr**: I couldn't find his email, but maybe **Mike** [can help](https://www.enterprise-ireland.com/en/About-Us/Our-People/board/Declan-Hughes.html)? |
| Siobhan Hamilton | Smarter Travel Team TFI |Smarter Travel Team TFI | Orlaith spoke with her over the phone during the hackathon. She advised emailing the department for transport. |
| Stephanie Anderson | AMCHAM | Public Affairs & Advocacy Manager | Government knowledge |
| Adrienne Harrington | Ludgate Hub in Skibbereen | CEO | Government knowledge & supporter of our mission |

* DAA contacts from Katie. Questions for Katie, deck. Call with Katie before 18th. Do work on the deck first.