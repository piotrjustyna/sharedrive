# data

## Air Pollution Monitoring Data Dublin City

https://data.gov.ie/dataset/air-pollution-monitoring-data-dublin-city - data from 2011...

## Noise maps from traffic sources in Dublin Region

https://data.gov.ie/dataset/noise-maps-from-traffic-sources-in-dublin-city-council - data from 2012...

## Noise Monitoring

https://data.gov.ie/dataset/noise_monitoring - this is pretty good. Example of drumcondra library noise: http://dublincitynoise.sonitussystems.com/applications/api/dublinnoisedata.php?location=1

## National Road Traffic Counts

* https://data.gov.ie/dataset/national-road-traffic-counts - Access Denied
* http://data.tii.ie/Datasets/TrafficCounters/

## Airport

75 data sets matching the airport keyword. Mostly useless to us, weather data.

### Noise Levels

http://data.gov.ie/dataset/noise-round-1-airport - can't get to data

### National Air Quality

http://gis.epa.ie/GetData/Download - need to find data

### TAM05 - Passengers, Freight and Commercial Flights by Airports in Ireland, Country, Direction, Flight Type, Month and Statistical

https://www.cso.ie/StatbankServices/StatbankServices.svc/jsonservice/responseinstance/TAM05 - This one is very interesting, but too granular for our purpose.

### DAA Year in Review - 2017

https://www.daa.ie/annual-report-2017/2017-year-in-review/ The report was published in April last year. 2018 data is not yet available.

* 31.9 million passengers (6% increase compared to 2017).
* During the period of 2014-2017, the number of passsengers increased by 51% in Dublin and Cork.
* Another major phase of capital investment with North Runway. Details: https://www.dublinairport.com/north-runway/about-north-runway/key-dates.
* Capacity enhancements at Dublin Airport (***Piotr: need details from Katie***).
* 2018-2021 strategy: "Creating Our Future". "To be airport industry leaders, growing our business with talented people delivering great service and value for airlines, passengers and business partners.".
* WIP...

## Carpooling - Insurance

https://www.aviva.ie/car-insurance/motor-advice/school-run-carpooling - not forbidden, premium not affected, it is in fact encouraged

https://www.theaa.ie/blog/everything-you-need-to-know-about-carpooling/ - not forbidden. As defines 3 rules for carpooling:
* Vehicle is not built or modified to carry more than 8 passengers, excluding the driver
* The passengers are not being carried as part of a business of carrying passengers
* Agreement is made prior to the journey commencing

Zurich is mostly the same as AA, but adds one more condition: the total contributions the insured receive do not mean that the insured financially 
profits from this arrangement. https://www.google.com/url?sa=t&source=web&rct=j&url=https://halligan.ie/wp-content/uploads/2015/09/Zurich-MotorPolicy.pdf&ved=2ahUKEwjCy6PexNbgAhUSmBQKHXqFBLcQFjAHegQICxAB&usg=AOvVaw07fF5gZxoq60FrzS3MBCq_

Aig, Allianz, Post - no mention of carpooling

## Competition in Ireland

https://www.google.com/url?sa=i&source=web&cd=&ved=2ahUKEwiErYuZx9bgAhUKxYUKHXA0DqkQzPwBegQIARAC&url=https%3A%2F%2Fwww.thejournal.ie%2Fride-sharing-ireland-2185650-Jun2015%2F&psig=AOvVaw2r2knYCDD6u4MgePbcTe4L&ust=1551172820951872&cshid=1551086420013

Carma is actually based in Cork, operates mostly in SF - break even payments.

WunderCar - optional tip the passengers can give. Quote: Last year the NTA issued a warning to German newcomer WunderCar that it would be breaking taxi licensing laws if it went ahead with plans to launch its ridesharing services in Dublin.
