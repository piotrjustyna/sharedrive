- [logo contact](#logo-contact)
- [gdpr](#gdpr)
    - [controller](#controller)
    - [processor](#processor)
- [data extraction](#data-extraction)
- [mvp](#mvp)

# logo contact

For a PoC they recommended we use a temp free icon (e.g. inconfinder). It can later be changed and we have plenty of time before our product actually makes it to the market.

# gdpr

No data controller I could find internally. However, did some research and it doesn't look like we'll need a data controller after all: https://www.dataprotection.ie/en/organisations. Terminology:

## controller

‘controller’ means the natural or legal person, public authority, agency or other body which, alone or jointly with others, determines the purposes and means of the processing of personal data;

## processor

‘processor’ means a natural or legal person, public authority, agency or other body which processes personal data on behalf of the controller;

# data extraction

https://bitbucket.org/piotrjustyna/sharedrive/src/master/20190227/Piotr_data.md

# mvp

* https://bitbucket.org/piotrjustyna/sharedrive/src/master/MVP.md
* https://bitbucket.org/piotrjustyna/sharedrive/src/master/20190227/ShareDrive.pdf
* https://bitbucket.org/piotrjustyna/sharedrive/src/master/20190227/ShareDrive.pptx

# GPS Tracking costs
few companies who provides GPS tracking apps, from 5 to 10 $$ per vehicle per month
https://blog.hubstaff.com/gps-tracker-app-options/

IBM creating API tool, free 30 days trial
https://www.ibm.com/ie-en/marketplace/api-management
IBM avarage cost per call is 0.14$, more calls less amount per call to be paid


