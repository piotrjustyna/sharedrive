# sharedrive

Sharedrive repository.

## mvp

* [MVP](https://bitbucket.org/piotrjustyna/sharedrive/src/master/MVP.md)

## contacts

* [List of contacts](https://bitbucket.org/piotrjustyna/sharedrive/src/master/contacts.md).

## technical questions

* [How to implement the check-in system?](resources/check-in_system/)