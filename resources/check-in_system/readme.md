# check-in system

| resource | description |
| --- | --- |
| [Inferring Person-to-person Proximity Using WiFi Signals](1610.04730.pdf) | While interesting, this is not a technology for us. It is based on the assumption that there will be a significant number of surrounding wifi devices allowing for proximity inference. |