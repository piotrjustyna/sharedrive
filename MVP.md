# introduction

There are some critical aspects of the idea we should clarify in order to comfortably pitch it to the potential investors. We also need a framework of real goals we can start implementing (https://en.m.wikipedia.org/wiki/SMART_criteria). The most critical one, in my opinion, is to clearly define a realistically deliverable product which at least partially addresses the carpooling problem.

## mvp

What should our MVP be?

This I would like to open for discussion, but I propose to divide the work into 5 stages (accompanied by relevant questions):

### vetting

* How should a vehicle registration process look like?
* How do similar processes look like (e.g. Uber)?
* How should driver vetting process look like?
* Who is responsible for correct driver authentication and data security of the process?
* Should passengers be vetted too, or should they remain anonymous?
* What extra insurance is needed?
* Who is liable should there be an incident?
* cost benefit analysis of vetting
* insured, not a new driver, car make, engine size - one refund for all.
* insurance, marketing opportunity for insurers. **Piotr**: verify with a broker.
* Uber contact: Anne-Marie.

### check-in/check-out

* How to prove that the driver is carpooling? Check-in/check-out system?
* Should the check-in/check-out be controlled only by the driver or by both: the driver and the passenger?
* How to verify that the driver you're about to travel with is allowed to carpool? E.g. taxi drivers have special identifiers. Potential for spoofing.
* How to verify that the vehicle the driver is driving is registered as a carpooling vehicle?
* constant internet connection - we decided it's required.

### tracking

* How and what to track? Distance driven? Fuel consumption: declared average/current (car API)?
* Are other factors relevant (air temperature, tyre pressure, rain, etc.)? Engine size?
* One size fits all.

### data transfer and retention

* How to send the tracked data to government API/servers?
* GDPR. Data controller. **Piotr/Emanuele**: data controller contact. What is considered identifiable data?
* How long should the data be stored for?

### refunds/other incentives

* Bus lanes: conditions of operation, contact for knowledge sharing and negotiations, how many extra cars could the bus lanes handle
* Tax refunds: rules (instant/once-yearly/other), contact for knowledge sharing and negotiations.
* Extra tax credits for carpooling drivers: rules, contact for knowledge sharing and negotiations.
* Toll charges: contact from toll companys, how to monitor, cost.
* Non-monetary incentives, contact for knowledge sharing and negotiations.
* Free Parking and/or Park and Ride areas.
* Change of plans for now: only focus on DAA.
* @all: How is this working in the Netherlands?